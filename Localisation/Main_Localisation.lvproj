﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="14008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="CheckHorozlWalls_2.vi" Type="VI" URL="../CheckHorozlWalls_2.vi"/>
		<Item Name="Compute_Y_Trans.vi" Type="VI" URL="../Compute_Y_Trans.vi"/>
		<Item Name="ComputeReducedCoords.vi" Type="VI" URL="../ComputeReducedCoords.vi"/>
		<Item Name="ComputeScale.vi" Type="VI" URL="../ComputeScale.vi"/>
		<Item Name="ComputeSimilarityTransform.vi" Type="VI" URL="../ComputeSimilarityTransform.vi"/>
		<Item Name="ComputeSinRotataion.vi" Type="VI" URL="../ComputeSinRotataion.vi"/>
		<Item Name="ConvertBeamToRobotSpace.vi" Type="VI" URL="../ConvertBeamToRobotSpace.vi"/>
		<Item Name="ConvertPointToWorldSpace_2.vi" Type="VI" URL="../ConvertPointToWorldSpace_2.vi"/>
		<Item Name="ConvertScanToWorldSpace.vi" Type="VI" URL="../ConvertScanToWorldSpace.vi"/>
		<Item Name="GetRotationAngle.vi" Type="VI" URL="../GetRotationAngle.vi"/>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="PC Main.vi" Type="VI" URL="../PC Main.vi"/>
		<Item Name="Pythag.vi" Type="VI" URL="../Pythag.vi"/>
		<Item Name="SinSum.vi" Type="VI" URL="../SinSum.vi"/>
		<Item Name="SquaredSum.vi" Type="VI" URL="../SquaredSum.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="instr.lib" Type="Folder">
				<Item Name="Hokuyo URG Series.lvlib" Type="Library" URL="/&lt;instrlib&gt;/Hokuyo URG Series/Hokuyo URG Series.lvlib"/>
			</Item>
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_AALPro.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALPro.lvlib"/>
			</Item>
			<Item Name="CheckVerticalWalls_2.vi" Type="VI" URL="../CheckVerticalWalls_2.vi"/>
			<Item Name="Compute_Trans_2.vi" Type="VI" URL="../Compute_Trans_2.vi"/>
			<Item Name="ComputeCosRotataion.vi" Type="VI" URL="../ComputeCosRotataion.vi"/>
			<Item Name="Control 1.ctl" Type="VI" URL="../Control 1.ctl"/>
			<Item Name="CosSum.vi" Type="VI" URL="../CosSum.vi"/>
			<Item Name="FindCofM.vi" Type="VI" URL="../FindCofM.vi"/>
			<Item Name="FindPointPairs.vi" Type="VI" URL="../FindPointPairs.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
